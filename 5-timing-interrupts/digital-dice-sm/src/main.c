#include <stdio.h>

#include "main.h"
#include "stm32f4xx.h"
#include "stm32f4xx_spi.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_rng.h"
#include "misc.h"
#include "stm32f4xx_syscfg.h"


#define ABS(x) ((x)<0 ? -(x) : (x))

#define BOUNCE_DELAY 100
#define THRESHOLD    80

#define STARTING 00
#define WAITING  01
#define SHAKING  10
#define DISPLAY  11

uint8_t state = STARTING;

/* The states are:
STARTING – during initialization
WAITING – waiting for first shake
SHAKING – while shaking the board
DISPLAYING – the result is being displayed on the LEDs
*/

const uint16_t LEDS = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;

uint32_t bounceCounter = 0;
int flag= 0;

static void initSystick();
void initAccelerometer();
void initLeds();
void initRng();

void readAxes();
uint8_t readSPI(uint8_t);
void writeSPI(uint8_t, uint8_t);

int8_t isShaking();

void showRandomNumber();

typedef struct {
	int16_t X;
	int16_t Y;
	int16_t Z;
} AccelerometerDataStruct;

AccelerometerDataStruct dat;
AccelerometerDataStruct datOld;

void initSystick() {
	// You can change the frequency of the SysTick interrupt if you want
	SysTick_Config(SystemCoreClock/1000);
}
 
void SysTick_Handler(void) { 

    switch(state) {
    case STARTING:
        // Fill in any necessary actions here
	state= STARTING;
        break;
    case WAITING: 
        // Fill in any necessary actions here
	readAxes(&datOld);	// read sensor and store into `datOld'
	for(bounceCounter= 0; bounceCounter< BOUNCE_DELAY; bounceCounter++){}
	//bounceCounter++;
	if(isShaking()){
		state= SHAKING;
		bounceCounter= 0;
	}
	else state= WAITING;	
        break;
    case DISPLAY:
        // Fill in any necessary actions here
	if(!flag){
		showRandomNumber();
		flag= 1;
	}
	readAxes(&datOld);	// read sensor and store into `datOld'
	for(bounceCounter= 0; bounceCounter< BOUNCE_DELAY; bounceCounter++){}
	//bounceCounter++;
	if(isShaking()){
		state= SHAKING;
		bounceCounter= 0;
		flag= 0;
	}
	else state= DISPLAY;
        break;
    case SHAKING:
        // Fill in any necessary actions here
	GPIO_ResetBits(GPIOD, LEDS);
	readAxes(&datOld);	// read sensor and store into `datOld'
	bounceCounter++;
	if(~isShaking() && bounceCounter > BOUNCE_DELAY){
		state= DISPLAY;
		bounceCounter= 0;
	}
	else state= SHAKING;	
        break;
    default:
	state = WAITING;
	break;
    }

}
 
int main() {
	SystemInit();

	initSystick();
	initRng();
	initAccelerometer();
	initLeds();

	state = WAITING;

	while(1);
}


void initRng() {
	
   RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);
   RNG_Cmd(ENABLE);

}

void initAccelerometer() {

	// Initialize SPI1 clock and data lines for slave (accelerometer)

	// Enable clock signal to SPI1 and GPIOA peripherals
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);


	// Configure GPIO pins for SPI1 clock and data lines
 	GPIO_InitTypeDef gpio_spi1;
	gpio_spi1.GPIO_OType = GPIO_OType_PP;
	gpio_spi1.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio_spi1.GPIO_Speed = GPIO_Speed_100MHz;
	gpio_spi1.GPIO_Mode = GPIO_Mode_AF;
	gpio_spi1.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_Init(GPIOA, &gpio_spi1);

	// Configure GPIO pins to use alternate function for SPI1
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

	// Configure SPI peripheral (master)
	SPI_InitTypeDef spi;

	SPI_StructInit(&spi);
	spi.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
	spi.SPI_DataSize = SPI_DataSize_8b;
	spi.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	spi.SPI_FirstBit = SPI_FirstBit_MSB;
	spi.SPI_Mode = SPI_Mode_Master;
	spi.SPI_CPOL = SPI_CPOL_High;
	spi.SPI_CPHA = SPI_CPHA_2Edge;
	spi.SPI_NSS = SPI_NSS_Soft;

	SPI_Init(SPI1, &spi);
	SPI_Cmd(SPI1, ENABLE);

	// Set up SPI chip select/slave select line
	GPIO_InitTypeDef gpio_cs;
	
	// Enable clock for CS GPIO port
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE); 
	
	// Configure CS pin
	gpio_cs.GPIO_Pin = GPIO_Pin_3;
	gpio_cs.GPIO_Mode = GPIO_Mode_OUT;
	gpio_cs.GPIO_OType = GPIO_OType_PP;
	gpio_cs.GPIO_PuPd = GPIO_PuPd_UP;
	gpio_cs.GPIO_Speed = GPIO_Speed_25MHz;
	GPIO_Init(GPIOE, &gpio_cs);

	// Set chip select line (high output)
	GPIO_SetBits(GPIOE, GPIO_Pin_3);
	
	//LIS302DL
	/* Reboot */
	uint16_t ctrl;
	ctrl= readSPI(LIS302DL_CTRL_REG2_ADDR);
	ctrl |= LIS302DL_BOOT_REBOOTMEMORY;
	writeSPI((uint8_t)LIS302DL_CTRL_REG2_ADDR, ctrl);
	/* Init settings */
	ctrl = (uint16_t) (LIS302DL_DATARATE_100 | LIS302DL_LOWPOWERMODE_ACTIVE | LIS302DL_SELFTEST_NORMAL | LIS302DL_XYZ_ENABLE);
	ctrl |= (uint16_t) LIS302DL_FULLSCALE_2_3;
	/* Write settings */
	writeSPI((uint8_t)LIS302DL_CTRL_REG1_ADDR, ctrl);
	/* Read filter */
	writeSPI((uint8_t)LIS302DL_CTRL_REG2_ADDR, ctrl);
	ctrl &= (uint8_t) ~(LIS302DL_FILTEREDDATASELECTION_OUTPUTREGISTER | LIS302DL_HIGHPASSFILTER_LEVEL_3 | LIS302DL_HIGHPASSFILTERINTERRUPT_1_2);
	/* Set filter */
	ctrl |= (uint8_t) (LIS302DL_HIGHPASSFILTERINTERRUPT_1_2 | LIS302DL_FILTEREDDATASELECTION_OUTPUTREGISTER);
	/* Set filter value */
	ctrl |= (uint8_t) LIS302DL_HIGHPASSFILTER_LEVEL_0;
	/* Write settings */
	writeSPI((uint8_t)LIS302DL_CTRL_REG2_ADDR, ctrl);
}


void writeSPI(uint8_t address, uint8_t data){
 
	// Copy your code from lab 4 here
	// set chip select line low (use GPIO_ResetBits)
	// CS: serial port enable, low: start transmission, high end transmission
	//LIS3DSH SPEC 6.2.2
	GPIO_ResetBits(GPIOE, GPIO_Pin_3);

	// The SPI_I2S_FLAG_TXE flag gives the status of the transmit buffer
	// register. Check its status with SPI_I2S_GetFlagStatus. 
	// When it is set, send 'address' using SPI1 with SPI_I2S_SendData
	
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI1, address);
	

	// The SPI_I2S_FLAG_RXNE flag gives the status of the receiver buffer 
	// register. Check its status with SPI_I2S_GetFlagStatus. 
	// When it is set, receive data with SPI_I2S_ReceiveData
	// (recall that SPI *requires* full duplex, so you should always 
	// receive a byte for each byte you send.)

	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	SPI_I2S_ReceiveData(SPI1);
	
	 
	// When the transmit buffer flag is set, send 
	// 'data' using SPI1
	
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI1, data); 

	// When the receive buffer flag is set, receive a byte 
	// over SPI1
	
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	SPI_I2S_ReceiveData(SPI1);

	// set chip select line high (use GPIO_SetBits)
	GPIO_SetBits(GPIOE, GPIO_Pin_3);

}


 
uint8_t readSPI(uint8_t address){

	// Copy your code from lab 4 here
	uint8_t data;
 
	// set chip select line low (use GPIO_ResetBits)
	//LIS3DSH SPEC 6.2.2
	GPIO_ResetBits(GPIOE, GPIO_Pin_3);
	 
	// Read operations require a 7-bit register address and bit 8 set to 1, so 
	// set `address` to `address` ORed with 1000 0000 (0x80 or 128) 
	// (See section 6.2.1 of the data sheet -
	// http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00040962.pdf
	// - for more details)
	address= 0x80 | address;

	// The SPI_I2S_FLAG_TXE flag gives the status of the transmit buffer
	// register. Check its status with SPI_I2S_GetFlagStatus. 
	// When it is set, send 'address' using SPI1 with SPI_I2S_SendData
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI1, address);

	// The SPI_I2S_FLAG_RXNE flag gives the status of the receiver buffer 
	// register. Check its status with SPI_I2S_GetFlagStatus. 
	// When it is set, receive data with SPI_I2S_ReceiveData
	// (recall that SPI *requires* full duplex, so you should always 
	// receive a byte for each byte you send.)
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	SPI_I2S_ReceiveData(SPI1);

	// When the transmit buffer flag is set, send 
	// '0x00' as a dummy byte using SPI1
	// (recall that SPI *requires* full duplex, so you should always 
	// send a byte for each byte you receive.)
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI1, 0x00);
 
	// When the receive buffer flag is set, receive a byte 
	// over SPI1. Save the byte in `data`
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	data= SPI_I2S_ReceiveData(SPI1);

	// set chip select line high (use GPIO_SetBits)
	GPIO_SetBits(GPIOE, GPIO_Pin_3);
 
	return data;
}
 

void readAxes(AccelerometerDataStruct *dat) {

	// Copy your code from lab 4 here
	// We get 1 byte at a time, and each axes data is 2 bytes.
	// We need a total of 6 bytes to deal with all three directions.
	int8_t axesValues[3];
	
	// Use readSPI to read in values from LIS3DSH_OUT_X_L_ADDR into axesValues[0]
	// and LIS3DSH_OUT_X_H_ADDR into axesValues[1]
	axesValues[0]= readSPI(LIS302DL_OUT_X_ADDR);


	// Use readSPI to read in values from LIS3DSH_OUT_Y_L_ADDR into axesValues[2]
	// and LIS3DSH_OUT_Y_H_ADDR into axesValues[3]
	axesValues[1]= readSPI(LIS302DL_OUT_Y_ADDR);
	

	// Use readSPI to read in values from LIS3DSH_OUT_Z_L_ADDR into axesValues[4]
	// and LIS3DSH_OUT_Z_H_ADDR into axesValues[5]
	axesValues[2]= readSPI(LIS302DL_OUT_Z_ADDR);


	// Use bit shifting to combine the two bytes for each axis into one 16-bit value
	// in the AccelerometerDataStruct that we passed by reference
	dat->X = (int16_t)(axesValues[0]) * LIS302DL_SENSITIVITY_2_3G;
	dat->Y = (int16_t)(axesValues[1]) * LIS302DL_SENSITIVITY_2_3G;
	dat->Z = (int16_t)(axesValues[2]) * LIS302DL_SENSITIVITY_2_3G;

}



void initLeds() {

    // Copy in your initLeds() function from lab2
	// Use RCC_AHB1PeriphClockCmd to enable the clock on GPIOD
	RCC_AHB1PeriphClockCmd(8, 1);
	


    // Declare a gpio as a GPIO_InitTypeDef:
    	GPIO_InitTypeDef gpio;

    // Call GPIO_StructInit, passing a pointer to gpio
    // This resets the GPIO port to its default values
	GPIO_StructInit(&gpio);

    // Before calling GPIO_Init, 
    // Use gpio.GPIO_Pin to set the pins you are interested in
    // (you can use the LEDS constant defined above)
	gpio.GPIO_Pin= LEDS;

    // Use gpio.GPIO_Mode to set these pins to output mode
	gpio.GPIO_Mode= GPIO_Mode_OUT; 

    // Now call GPIO_Init with the correct arguments
	GPIO_Init(GPIOD, &gpio);


}



void showRandomNumber() {
	GPIO_ResetBits(GPIOD, LEDS);

        /* Wait for the data to appear.*/
        while(RESET == RNG_GetFlagStatus(RNG_FLAG_DRDY)){}
	// Don't let the number be zero
	int n = 0;
 	while (!n) {
		// Use 4 bits of random number to set LEDs
		n = RNG_GetRandomNumber() & 0xf << 12;
	}
	GPIO_SetBits(GPIOD, n);
	setbuf(stdout, NULL);
	printf("Number: %d\n", RNG_GetRandomNumber() & 0xf);	// the member variables for each direction
}

int8_t isShaking() {
	readAxes(&dat);	// read sensor and store into `dat'
	if ((ABS(dat.Y - datOld.Y)) > THRESHOLD) {
		return 1;
	}
	else return 0;
}
