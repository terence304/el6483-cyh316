#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_adc.h"
#include "misc.h"
#include "stm32f4xx_syscfg.h"

#include "rgbled.h"
#include "lightsensor.h"
#include "bluetooth.h"
#include "leds.h"
#include "power.h"
#include "buffer.h"
#include "audioutils.h"

#include <stdio.h>

/* Use flags to keep track of system state. Set flags in ISRs, then check their 
   values and act accordingly in the main while() loop.                        */
int flags = 0;
int btflag= 0;
int sendflag= 0;
int light= 0;
int night= 0;
char sendWord= '0';
char charRead= '0';
uint8_t r= 0xFF;
uint8_t g= 0xFF;
uint8_t b= 0xFF;
uint8_t v= 0x00;

/* Circular buffer used for incoming Bluetooth data */
buffer rxBuffer;

/* Set up other global variables here              */
const uint16_t LEDS = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
uint32_t risingCounter= 0;
uint32_t fallingCounter= 0;

/**
 * @brief main() implements main system logic
 */
int main() {
    
    initBuffer(&rxBuffer);
    /* Initialize everything else that needs initializing here  */
    initLeds();
    initAdc();
    initPwm();
    initUsart();
    initBtState();
    initWakeupPin();

    //testLeds();	
    testLightsensor();
    //testrgbled();
    //testbluetooth();
    //testbuffer(rxBuffer);
    testpower();
    //testmusic();

    setColor(0x00, 0x00, 0x00);

    while (1) {
	light = getLightReading();
	if(0== flags){                            //save energy
		//setColor(0x00, 0x00, 0x00);
		testpower();
		GPIO_SetBits(GPIOD, GPIO_Pin_15);
		GPIO_SetBits(GPIOD, GPIO_Pin_14);
		testmusic();
	}
	else if(1== flags){                       //play music
		if(night){
			GPIO_SetBits(GPIOD, LEDS);
		}
		else{
			GPIO_ResetBits(GPIOD, LEDS);
		}		
				
		testmusic();
	}
	else if(2== flags){                       //bluetooth off
		GPIO_ResetBits(GPIOD, GPIO_Pin_15);
		GPIO_ResetBits(GPIOD, GPIO_Pin_14);
		//testmusic();
	}
	else if(3== flags){                       //bluetooth on
		if(1== sendflag){
		charRead= read(&rxBuffer);
		setAdcThresholds(LIGHT_THRESHOLD_HIGH, LIGHT_THRESHOLD_LOW);
		printf("sensor on\n");
		}
		else if(2== sendflag){
		charRead= read(&rxBuffer);
		setAdcThresholds(5000, 0);
		printf("sensor off\n");
		}
		else if(3== sendflag){
		charRead= read(&rxBuffer);
		while(size(&rxBuffer)<3);
		r= read(&rxBuffer);
		g= read(&rxBuffer);
		b= read(&rxBuffer);
		setColor(r, g, b);
		printf("set color\n");
		}
		else if(4== sendflag){
		charRead= read(&rxBuffer);
		while(size(&rxBuffer)<1);
		v= read(&rxBuffer);
		setMusicVolume(v);
		printf("set volume\n");	
		}
		else{
		sendflag= 0;
		}	
	}
	
    }  
}



/**
 * @brief Interrupt handler for user button (used for wakeup from stop mode)
 * 
 * If you have previously called initWakeupPin(), this handler will be
 * triggered when the User button is pressed (rising edge) or other external 
 * interrupt. 
 */
void EXTI0_IRQHandler(void) {
    if(EXTI_GetITStatus(EXTI_Line0) != RESET){
        EXTI_ClearITPendingBit(EXTI_Line0);
	// do something
	if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)){
		GPIO_SetBits(GPIOD, LEDS);
		for(int x= 0; x< 10000; x++){
			GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0);
		}
		risingCounter++;
		
	}
	if(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)){
		GPIO_ResetBits(GPIOD, LEDS);
		for(int x= 0; x< 10000; x++){
			GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0);
		}
		fallingCounter++;	
	}
	flags= 1;	
	SystemInit();
	
     }
} 


/**
 * @brief Interrupt handler for Bluetooth status changes
 * 
 * If you have previously called initBtState(), this handler will be
 * triggered when a Bluetooth connection is made or broken.
 */
void EXTI1_IRQHandler(void) {
    if(EXTI_GetITStatus(EXTI_Line1) != RESET){
      EXTI_ClearITPendingBit(EXTI_Line1);
      // Do something on Bluetooth connection status change
      btflag= btConnected();
      if(btflag){
		printf("bt connected\n");
		flags= 3;
      }
      else{
		printf("bt disconnected");
		flags= 2;
      }
      GPIO_SetBits(GPIOD, GPIO_Pin_12);
    }
} 


/**
 * @brief Interrupt handler for USART1 (Bluetooth) interrupts
 *
 * If you have previously called initUsart(), this handler will receive 
 * data from the RX port and write it to the circular buffer rxBuffer.
 */
void USART1_IRQHandler(void){

	setbuf(stdout, NULL);
 
  if( USART_GetITStatus(USART1, USART_IT_RXNE) ){
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);
	sendWord= USART1->DR;
	write(&rxBuffer, (uint8_t)USART1->DR);
	
	if('S' == sendWord){
		sendflag= 1;
	}
	else if('R' == sendWord){
		sendflag= 2;
	}
	else if('C' == sendWord){
		sendflag= 3;
	}
	else if('V' == sendWord){
		sendflag= 4;
	}
	else if('0' == sendWord){
		flags= 0;
		printf("power off\n");
	}
  }
}


/**
 * @brief Interrupt handler for ADC1 interrupts
 *
 * If you have previously called initAdc(), this handler will be called
 * when the light reading is higher than LIGHT_THRESHOLD_HIGH or lower 
 * than LIGHT_THRESHOLD_LOW. 
 */
void ADC_IRQHandler(void) {
    if(ADC_GetITStatus(ADC1, ADC_IT_AWD) != RESET) {
     ADC_ClearITPendingBit(ADC1, ADC_IT_AWD);
     // do something
	//int x= getLightReading();
	//printf("%d\n", x);
	if(light > LIGHT_THRESHOLD_HIGH){
		night= 1;
        }
	if(light < LIGHT_THRESHOLD_LOW){
		night= 0;
        }
    }
}
