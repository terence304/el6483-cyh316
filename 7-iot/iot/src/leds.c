#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

#define LEDS  GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15

/**
 * @brief Set up on-board LEDs as GPIO outputs
 *
 * This function initializes the GPIO output pins connected 
 * to the four on-board LEDs.
 */
void initLeds() {

  // Copy this function in from lab 2
  // Use RCC_AHB1PeriphClockCmd to enable the clock on GPIOD
	RCC_AHB1PeriphClockCmd(8, 1);
	
    // Declare a gpio as a GPIO_InitTypeDef:
    	GPIO_InitTypeDef gpio;

    // Call GPIO_StructInit, passing a pointer to gpio
    // This resets the GPIO port to its default values
	GPIO_StructInit(&gpio);

    // Before calling GPIO_Init, 
    // Use gpio.GPIO_Pin to set the pins you are interested in
    // (you can use the LEDS constant defined above)
	gpio.GPIO_Pin= LEDS;

    // Use gpio.GPIO_Mode to set these pins to output mode
	gpio.GPIO_Mode= GPIO_Mode_OUT; 

    // Now call GPIO_Init with the correct arguments
	GPIO_Init(GPIOD, &gpio);	
}

void testLeds(){
	GPIO_SetBits(GPIOD, LEDS);
}
