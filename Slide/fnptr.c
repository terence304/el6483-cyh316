// This sample function pointer code came from
// http://codeandlife.com/2013/10/06/tutorial-state-machines-with-c-callbacks/

// States
void led_on();
void led_off();

// State pointer
void (*statefunc)() = led_on;

void led_on() {
  PORTB |= 1;
  statefunc = led_off;
}

void led_off() {
  PORTB &= ~1;
  statefunc = led_on;
}

int main() {
  DDRB |= 1;

  while(1) {
    (*statefunc)();
    _delay_ms(1000); // sleep for a second
  }

  return 1; // should not get here
}

