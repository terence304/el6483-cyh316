Lab 1: Blinky
====================

Lab Assignment | 1 - Blinky
-------------- | -------------
Name           |Terence
Net ID         |cyh316
Report due     | Tuesday, 17 February


Please answer the following questions:

1) The Makefile for this lab exercise defines, among other things, the compiler to use. (`CC` is a common shorthand used in makefiles for the compiler.) What is the name of the compiler used for this lab exercise?

arm-none-eabi-gcc


2) The Makefile also defines certain *flags* that define compiler behavior. Fill in the following table, matching the flag to the behavior. The first one is completed for you as an example:

Behavior                                  | Flag
----------------------------------------- | -------------
Include debug info for GDB                | `-ggdb`
Compile for a little-endian target        |-mlittle-endian
Use the Thumb instruction set             |-mthumb 
Compile for a target with a Cortex M4 CPU |-mcpu=cortex-m4


3) The Makefile also instructs the linker to link files for STM libraries from three locations. What are those locations? (Give the full file path, not the line with variables given in the Makefile.)
/home/terence/el6483-cyh316/1-blinky/src/STM32F4-Discovery_FW_V1.1.0/Utilities/STM32F4-Discovery
/home/terence/el6483-cyh316/1-blinky/src/STM32F4-Discovery_FW_V1.1.0/Libraries/CMSIS/Include -I$(STM_COMMON)/Libraries/CMSIS/ST/STM32F4xx/Include
/home/terence/el6483-cyh316/1-blinky/src/STM32F4-Discovery_FW_V1.1.0/Libraries/STM32F4xx_StdPeriph_Driver/inc


4) The Makefile includes information about a startup file, which will be added to the executable file. Startup code is run just after a microcontroller is reset, before the main program. It's usually implemented as universal code for all same microcontroller type, so you won't need to write one from scratch for each program.

What is the location of the startup code? (Give the full file path, not the line with variables given in the Makefile.)
/home/terence/el6483-cyh316/1-blinky/src/STM32F4-Discovery_FW_V1.1.0/Libraries/CMSIS/ST/STM32F4xx/Source/Templates/TrueSTUDIO/startup_stm32f4xx.s 

What form is the startup code given in? (C code, assembly, binary?)
assembly

5) There are five commands in the .gdbinit file. Look up each command online or in the `gdb` manual. List all five commands here, and explain what each one does.
target extended localhost:3333
monitor arm semihosting enable
monitor reset halt
load
monitor reset init

6) What is in each of the ARM registers before execution starts?  Give the output
of `info registers` in the code block below.
r0             0x0      0
r1             0x0      0
r2             0x0      0
r3             0x0      0
r4             0x0      0
r5             0x0      0
r6             0x0      0
r7             0x0      0
r8             0x0      0
r9             0x0      0
r10            0x0      0
r11            0x0      0
r12            0x0      0
sp             0x20020000       0x20020000
lr             0xffffffff       -1
pc             0x80010f8        0x80010f8 <Reset_Handler>
xPSR           0x1000000        16777216
```

```


7) Copy the assembly code for the `SysTick_Handler` function as shown by `gdb`
into the code block below.
0x080003a8 <+0>:     push    {r7}
   0x080003aa <+2>:     add     r7, sp, #0
=> 0x080003ac <+4>:     ldr     r3, [pc, #16]   ; (0x80003c0 <SysTick_Handler+24
>)
   0x080003ae <+6>:     ldr     r3, [r3, #0]
   0x080003b0 <+8>:     adds    r2, r3, #1
   0x080003b4 <+12>:    str     r2, [r3, #0]
   0x080003b6 <+14>:    mov     sp, r7
   0x080003b8 <+16>:    ldr.w   r7, [sp], #4
   0x080003bc <+20>:    bx      lr
   0x080003be <+22>:    nop
   0x080003c0 <+24>:    lsrs    r4, r0, #6
   0x080003c2 <+26>:    movs    r0, #0

```

```

8) Include a screenshot showing the "Toggling LED" output in your `openocd` window.
You can use any image hosting service to put your screenshot online.

![](http://url/of/image)

9) Include a screenshot showing the "Toggling LED from on to off"
and "Toggling LED from off to on" output in your `openocd` window.
You can use any image hosting service to put your screenshot online.

![](http://url/of/image)


10) If you stop `openocd` and `gdb`, and disconnect and reconnect the board from
the host computer, will the program (as given) keep running?
Explain the behavior you observe. What about your modified 'nodebug' program?
