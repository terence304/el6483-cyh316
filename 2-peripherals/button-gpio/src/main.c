#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"


void init();
void loop();

void initLeds();
void initButton();


int main() {
    init();

    do {
        loop();
    } while (1);
}

void init() {
    initLeds();
    initButton();
}

void loop() {

    if (GPIOA->IDR & 1) {
        // Use BSRRL, set bits of GPIOD pins 12-15
	GPIOD->BSRRL |= (0xF << 12);

    }
    else {
        // Use BSRRH, set bits of GPIOD pins 12-15
	GPIOD->BSRRH |= (0xF << 12);

    }
}

void initLeds() {
    // Enable GPIOD Clock
	RCC->AHB1ENR |= 8; 

    // Set GPIOD pins 12-15 mode to output
	GPIOD->MODER |= (0x55 << (12<<1));
	

}

void initButton() {
    // Enable GPIOA Clock
	RCC-> AHB1ENR |= 1;

    // Set GPIOA pin 0 mode to input
	GPIOA->MODER &= 0xfffffffc;

}

